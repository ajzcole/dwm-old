/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx    = 2;       /* border pixel of windows */
static const unsigned int snap        = 5;       /* snap pixel */
static const int swallowfloating      = 0;       /* 1 means swallow floating windows by default */
static const unsigned int baralpha    = 0xb3;    /* transparancy of status bar */
static const unsigned int borderalpha = 0xb3;    /* transparancy of window borders */
static const unsigned int gappih      = 15;      /* horiz inner gap between windows */
static const unsigned int gappiv      = 15;      /* vert inner gap between windows */
static const unsigned int gappoh      = 25;      /* horiz outer gap between windows and screen edge */
static const unsigned int gappov      = 25;      /* vert outer gap between windows and screen edge */
static const int smartgaps            = 0;       /* 1 means no outer gap when there is only one window */
static const unsigned int statusrpad  = 10;      /* padding right of status text */
static const unsigned int taglpad     = 0;       /* padding left of tags */
static const int ulstatus             = 0;       /* 1 means underline status items */
static const int showbar              = 1;       /* 0 means hide bar by default */
static const int topbar               = 0;       /* 0 means bottom bar */
static const int vertpad              = 0;       /* vertical padding of bar */
static const int sidepad              = 0;       /* horizontal padding of bar */
static const int usebarpadtop         = 0;
static const char *fonts[]            = { "Inconsolata:size=11", "Material Design Icons Desktop:size=12" };
/* static const char dmenufont[]         = "Product Sans:size=12"; */
static const char col_normfg[]        = "#EEEEEE";
static const char col_normbg[]        = "#000000";
static const char col_selfg[]         = "#EEEEEE";
static const char col_selbg[]         = "#111111";
static const char *colors[][3]        = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_normfg, col_normbg, col_normbg },
	[SchemeSel]  = { col_selfg, col_selbg, col_normfg },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class	instance  	title		tags mask	isfloating	isterminal	noswallow	monitor	float x,y,w,h	floatborderpx*/
	{ NULL,		NULL,		"lf",		0,		0,          	1,		0,		-1,	0,0,0,0,	2 },
	{ NULL,		NULL,		"alsamixer",	0,		1,          	0,		0,		-1,	30,70,400,300,	2 },
	{ NULL,		NULL,		"frontcam",	~0,		1,          	0,		0,		-1,	0,460,408,306,	0 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,		{.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      toggletag,	{.ui = 1 << TAG} },
/*	{ MODKEY|ControlMask,           KEY,      toggleview,	{.ui = 1 << TAG} }, */
/*	{ MODKEY|ControlMask|ShiftMask, KEY,      tag,		{.ui = 1 << TAG} }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
/* static char dmenumon[2] = "0"; /-* component of dmenucmd, manipulated in spawn() *-/
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL }; */

static Key keys[] = {
	/* modifier                     key        function        argument */
/*	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } }, */
/*	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } }, */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      pushdown,       {0} },
	{ MODKEY|ShiftMask,             XK_k,      pushup,         {0} },
	{ MODKEY|ShiftMask,             XK_h,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_l,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_slash,  togglegaps,     {0} },
	{ MODKEY,                       XK_bracketleft,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_bracketright, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_bracketleft,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_bracketright, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
/*	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } }, */
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

